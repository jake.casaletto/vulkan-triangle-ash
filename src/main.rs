use std::io::Write;

use triangle::*;

fn main() {
    let vertex1 = AppVertex {
        position: [-0.5, -0.5],
    };
    let vertex2 = AppVertex {
        position: [0.0, 0.5],
    };
    let vertex3 = AppVertex {
        position: [0.5, -0.25],
    };

    let run_result = run(vec![vertex1, vertex2, vertex3]);
    let _ = match run_result {
        Ok(_) => std::io::stdout().write_all(b"Finished."),
        Err(e) => std::io::stderr().write_all(format!("{:?}", e).as_bytes()),
    };
}
