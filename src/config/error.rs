use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Error creating Config directory: {0}")]
    DirectoryCreation(String),
    #[error("Error creating Config file: {0}")]
    FileCreation(String),
    #[error("Error reading Config file: {0}")]
    FileRead(String),
    #[error("Error deserializing Config file: {0}")]
    Deserialization(String),
    #[error("Error serializing Config file: {0}")]
    Serialization(String),
}
