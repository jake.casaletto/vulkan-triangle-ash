use std::sync::Arc;

use vulkano::{
    command_buffer::CommandBufferExecFuture,
    device::DeviceExtensions,
    pipeline::graphics::viewport::Viewport,
    swapchain::{AcquireError, PresentFuture, SwapchainAcquireFuture, SwapchainPresentInfo},
    sync::{
        self,
        future::{FenceSignalFuture, JoinFuture, NowFuture},
        FlushError, GpuFuture,
    },
};

use crate::{config::Config, main_loop::MainLoop, vertex::AppVertex};

use self::{
    buffer::{
        command::{VulkanCommandBuffer, VulkanCommandBufferAllocator},
        frame::VulkanFrameBuffer,
        vertex::VulkanVertexBuffer,
    },
    device::{logical::VulkanDevice, physical::VulkanPhysicalDevice},
    error::{Error, Result},
    instance::VulkanInstance,
    pipeline::VulkanGraphicsPipeline,
    render_pass::VulkanRenderPass,
    shaders::VulkanShaders,
    surface::VulkanSurface,
    swapchain::VulkanSwapchain,
};
use vulkano::swapchain::acquire_next_image;

pub mod buffer;
pub mod device;
pub mod error;
pub mod instance;
pub mod pipeline;
pub mod render_pass;
pub mod shaders;
pub mod surface;
pub mod swapchain;

#[derive(Default)]
pub struct VulkanRenderState {
    pub recreate_swapchain: bool,
    pub window_resized: bool,
    pub fences: Vec<
        Option<
            Arc<
                FenceSignalFuture<
                    PresentFuture<
                        CommandBufferExecFuture<
                            JoinFuture<Box<dyn GpuFuture>, SwapchainAcquireFuture>,
                        >,
                    >,
                >,
            >,
        >,
    >,
    pub previous_render_frame_index: u32,
}

pub struct VulkanRenderer {
    pub instance: VulkanInstance,
    pub surface: VulkanSurface,
    pub physical_device: VulkanPhysicalDevice,
    pub logical_device: VulkanDevice,
    pub compiled_shaders: VulkanShaders,
    pub swapchain: VulkanSwapchain,
    pub render_pass: VulkanRenderPass,
    pub graphics_pipeline: VulkanGraphicsPipeline,
    pub command_buffer_allocator: VulkanCommandBufferAllocator,
    pub command_buffer: Option<VulkanCommandBuffer>,
    pub frame_buffer: Option<VulkanFrameBuffer>,
    pub vertex_buffer: Option<VulkanVertexBuffer>,
    pub state: VulkanRenderState,
    pub viewport: Viewport,
}

impl VulkanRenderer {
    pub fn try_new(config: &Config, main_loop: &MainLoop) -> Result<Self> {
        let required_device_extensions = &DeviceExtensions {
            khr_swapchain: true,
            ..Default::default()
        };
        let vulkan_instance = VulkanInstance::try_new(config)?;
        let surface = VulkanSurface::try_new(main_loop, config, &vulkan_instance)?;

        let physical_device =
            VulkanPhysicalDevice::try_new(&vulkan_instance, &surface, &required_device_extensions)?;
        let logical_device = VulkanDevice::try_new(&physical_device, &required_device_extensions)?;

        let compiled_shaders = VulkanShaders::try_new(&logical_device)?;
        let swapchain = VulkanSwapchain::try_new(&surface, &physical_device, &logical_device)?;
        let default_viewport = Viewport {
            origin: [0.0, 0.0],
            dimensions: [
                swapchain.expose_inner_TODO_REMOVE().image_extent()[0] as f32,
                swapchain.expose_inner_TODO_REMOVE().image_extent()[1] as f32,
            ],
            depth_range: 0.0..1.0,
        };
        let render_pass = VulkanRenderPass::try_new(&swapchain, &logical_device)?;
        let graphics_pipeline = VulkanGraphicsPipeline::try_new(
            &swapchain,
            &compiled_shaders,
            &logical_device,
            &render_pass,
            &default_viewport,
        )?;

        let command_buffer_allocator = VulkanCommandBufferAllocator::new(&logical_device);
        let renderer = Self {
            instance: vulkan_instance,
            surface,
            physical_device,
            logical_device,
            compiled_shaders,
            swapchain,
            render_pass,
            graphics_pipeline,
            command_buffer_allocator,
            command_buffer: None,
            frame_buffer: None,
            vertex_buffer: None,
            state: Default::default(),
            viewport: default_viewport,
        };
        Ok(renderer)
    }

    pub fn recreate_swapchain(&mut self) -> Result<()> {
        let new_chain = VulkanSwapchain::recreate_swapchain(&self.swapchain, &self.surface)?;
        self.swapchain = new_chain;
        Ok(())
    }

    pub fn create_frame_buffers(&mut self) -> Result<()> {
        let frame_buffers = VulkanFrameBuffer::try_new(&self.swapchain, &self.render_pass)?;
        self.frame_buffer = Some(frame_buffers);
        Ok(())
    }

    pub fn create_vertex_buffer(&mut self, vertices: Vec<AppVertex>) -> Result<()> {
        let vertex_buffer = VulkanVertexBuffer::try_new(&self.logical_device, vertices)?;
        self.vertex_buffer = Some(vertex_buffer);
        Ok(())
    }

    pub fn recreate_pipeline(&mut self) -> Result<()> {
        let new_dimensions = self
            .surface
            .try_expose_inner_window_TODO_REMOVE()?
            .inner_size();
        self.viewport.dimensions = new_dimensions.into();
        let new_pipeline = VulkanGraphicsPipeline::try_new(
            &self.swapchain,
            &self.compiled_shaders,
            &self.logical_device,
            &self.render_pass,
            &self.viewport,
        )?;
        self.graphics_pipeline = new_pipeline;
        Ok(())
    }

    pub fn create_command_buffer(&mut self) -> Result<()> {
        let new_command_buffer = VulkanCommandBuffer::new(
            &self.command_buffer_allocator,
            &self.logical_device,
            &self.graphics_pipeline,
            &self
                .frame_buffer
                .as_mut()
                .expect("Tried to create command buffer with no frame buffers"),
            &self
                .vertex_buffer
                .as_mut()
                .expect("Tried to create command buffer with no vertex buffer"),
        );
        self.command_buffer = Some(new_command_buffer);
        Ok(())
    }

    pub fn render_next_image(&mut self) -> Result<()> {
        match acquire_next_image(self.swapchain.expose_inner_TODO_REMOVE().clone(), None) {
            Ok((image_i, suboptimal, acquire_future)) => {
                if suboptimal {
                    self.state.recreate_swapchain = true;
                }

                if self
                    .state
                    .fences
                    .len()
                    .lt(&self.swapchain.expose_inner_images_TODO_REMOVE().len())
                {
                    let mut appended_vec =
                        vec![
                            None;
                            self.swapchain.expose_inner_images_TODO_REMOVE().len()
                                - self.state.fences.len()
                        ];
                    self.state.fences.append(&mut appended_vec);
                }

                if let Some(image_fence) = &self.state.fences[image_i as usize] {
                    image_fence
                        .wait(None)
                        .map_err(|e| Error::SwapchainImageAccess(format!("{:?}", e)))?;
                }

                let previous_future = match self.state.fences
                    [self.state.previous_render_frame_index as usize]
                    .clone()
                {
                    Some(s) => s.boxed(),
                    None => {
                        let mut now =
                            sync::now(self.logical_device.expose_inner_TODO_REMOVE().clone());
                        now.cleanup_finished();

                        now.boxed()
                    }
                };

                let execution = previous_future
                    .join(acquire_future)
                    .then_execute(
                        self.logical_device
                            .expose_inner_queues_TODO_REMOVE()
                            .first()
                            .expect("Could not access device queue")
                            .clone(),
                        self.command_buffer
                            .as_mut()
                            .expect("No command buffer allocated!")
                            .expose_inner_TODO_REMOVE()[image_i as usize]
                            .clone(),
                    )
                    .unwrap()
                    .then_swapchain_present(
                        self.logical_device
                            .expose_inner_queues_TODO_REMOVE()
                            .first()
                            .expect("Could not access device queue")
                            .clone(),
                        SwapchainPresentInfo::swapchain_image_index(
                            self.swapchain.expose_inner_TODO_REMOVE().clone(),
                            image_i,
                        ),
                    )
                    .then_signal_fence_and_flush();

                let fence = match execution {
                    Ok(ex) => Some(Arc::new(ex)),
                    Err(FlushError::OutOfDate) => {
                        self.state.recreate_swapchain = true;
                        None
                    }
                    Err(_) => None,
                };

                self.state.fences[image_i as usize] = fence;
                self.state.previous_render_frame_index = image_i;

                Ok(())
            }
            Err(AcquireError::OutOfDate) => {
                self.state.recreate_swapchain = true;
                Ok(())
            }
            Err(e) => Err(Error::SwapchainImageAccess(format!("{:?}", e))),
        }
    }
}
