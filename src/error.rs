use thiserror::Error;

use crate::config;
use crate::vulkan;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    ConfigError(#[from] config::Error),
    #[error(transparent)]
    VulkanError(#[from] vulkan::error::Error),
}
