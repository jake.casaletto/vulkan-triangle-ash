use winit::{
    event::{ElementState, Event, KeyboardInput, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
};

use crate::{vertex::AppVertex, vulkan::VulkanRenderer};

pub struct MainLoop {
    _inner_events: EventLoop<()>,
    _vertices: Vec<AppVertex>,
}

impl MainLoop {
    pub fn new() -> Self {
        Self {
            _inner_events: EventLoop::new(),
            _vertices: vec![],
        }
    }

    pub fn run_loop(mut self, mut renderer: VulkanRenderer, vertices: Vec<AppVertex>) {
        self._vertices = vertices.clone();

        renderer
            .create_frame_buffers()
            .expect("Could not create frame buffers");
        renderer
            .create_vertex_buffer(vertices.clone())
            .expect("Could not create vertex buffer");
        renderer
            .create_command_buffer()
            .expect("Could not create command buffer");

        self._inner_events.run(move |event, _, control_flow| {
            if let Some(new_control_flow) =
                Self::handle_event(event, &mut renderer, self._vertices.clone())
            {
                *control_flow = new_control_flow;
            }
        })
    }

    // TODO - inject handlers as dependencies and don't couple with main loop
    fn handle_event(
        event: Event<'_, ()>,
        renderer: &mut VulkanRenderer,
        vertices: Vec<AppVertex>,
    ) -> Option<ControlFlow> {
        match event {
            Event::WindowEvent { event, .. } => Self::handle_window_event(event, renderer),
            Event::MainEventsCleared => {
                Self::create_new_frames(renderer, vertices);
                Self::render(renderer);
                None
            }
            _ => None,
        }
    }

    fn render(renderer: &mut VulkanRenderer) {
        renderer
            .render_next_image()
            .expect("Failed to create and render next image!");
    }

    fn create_new_frames(renderer: &mut VulkanRenderer, vertices: Vec<AppVertex>) {
        if renderer.state.recreate_swapchain || renderer.state.window_resized {
            renderer.state.recreate_swapchain = false;
            renderer
                .recreate_swapchain()
                .expect("Could not recreate swapchain");
            renderer
                .create_frame_buffers()
                .expect("Could not create frame buffers");
            renderer
                .create_vertex_buffer(vertices.clone())
                .expect("Could not create vertex buffer");

            if renderer.state.window_resized {
                renderer.state.window_resized = false;
                renderer
                    .recreate_pipeline()
                    .expect("Could not recreate pipeline after window resize");
                renderer
                    .create_command_buffer()
                    .expect("Could not recreate command buffer after window resize");
            }
        }
    }

    // TODO - inject handlers as dependencies and don't couple with main loop
    fn handle_window_event(
        window_event: WindowEvent<'_>,
        renderer: &mut VulkanRenderer,
    ) -> Option<ControlFlow> {
        match window_event {
            WindowEvent::CloseRequested => Some(ControlFlow::Exit),
            WindowEvent::KeyboardInput { input, .. } => Self::handle_keyboard_input(input),
            WindowEvent::Resized(_dimensions) => {
                renderer.state.window_resized = true;
                None
            }
            _ => None,
        }
    }

    // TODO - inject handlers as dependencies and don't couple with main loop
    fn handle_keyboard_input(keyboard_input: KeyboardInput) -> Option<ControlFlow> {
        // TODO - abstract all input handling to a remappable library (device / keymap agnostic)
        if keyboard_input.state == ElementState::Pressed
            && keyboard_input.virtual_keycode == Some(winit::event::VirtualKeyCode::Escape)
        {
            return Some(ControlFlow::Exit);
        }
        None
    }

    // TODO abstract all winit dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &EventLoop<()> {
        &self._inner_events
    }
}
