#![allow(
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::non_ascii_literal,
    clippy::wildcard_imports,
    clippy::module_name_repetitions,
    incomplete_features
)]
#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    clippy::unreadable_literal,
    rust_2018_idioms
)]
#![deny(
    clippy::pedantic,
    clippy::float_cmp_const,
    clippy::unwrap_used,
    clippy::future_not_send
)]
#![forbid(bare_trait_objects)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

mod config;
mod main_loop;
mod vertex;
mod vulkan;

mod error;
pub use error::{Error, Result};

use config::Config;
use main_loop::MainLoop;
pub use vertex::AppVertex;
use vulkan::VulkanRenderer;

// TODO - use idiomatic config directory for OS instead of local relative folder creation
pub const CONFIG_DIRECTORY: &str = "triangle_config";
pub const CONFIG_FILENAME: &str = "config.json";

pub fn run(vertices: Vec<AppVertex>) -> Result<()> {
    let loaded_config = Config::load(CONFIG_DIRECTORY, CONFIG_FILENAME)?;
    let main_loop = MainLoop::new();
    let renderer = VulkanRenderer::try_new(&loaded_config, &main_loop)?;
    main_loop.run_loop(renderer, vertices);
    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn function_under_test__action_test__result() {
        // Given
        let i = 1 + 1;
        // When
        let j = 2;
        // Then
        assert_eq!(i, j);
    }
}
