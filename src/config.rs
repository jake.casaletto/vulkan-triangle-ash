mod error;
pub use error::{Error, Result};

use std::{
    fs,
    io::{BufWriter, Write},
};

// TODO decouple configs
#[derive(serde::Serialize, serde::Deserialize)]
pub struct Config {
    // Window
    pub window_x: u32,
    pub window_y: u32,
    pub window_title: String,
    // App
    // pub application_name: String,
    // pub application_version_major: u32,
    // pub application_version_minor: u32,
    // pub application_version_patch: u32,

    // Engine
    // pub engine_name: String,
    // pub engine_version_major: u32,
    // pub engine_version_minor: u32,
    // pub engine_version_patch: u32,

    // Vulkan
    pub vulkan_enable_validation_layers: bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            window_title: "Vulkano Book Triangle".to_string(),
            window_x: 800,
            window_y: 600,
            // application_name: "Triangle".to_string(),
            // application_version_major: 0,
            // application_version_minor: 0,
            // application_version_patch: 1,

            // engine_name: "Domino Engine".to_string(),
            // engine_version_major: 0,
            // engine_version_minor: 0,
            // engine_version_patch: 1,

            // TODO - remove config assertions and move to some sort of build cli
            #[cfg(all(debug_assertions))]
            vulkan_enable_validation_layers: true,
            #[cfg(not(debug_assertions))]
            vulkan_enable_validation_layers: false,
        }
    }
}

// TODO - move the ser/de + file io behind an impld trait for reuse with other serialized items.
impl Config {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn load(dir: &str, filename: &str) -> Result<Self> {
        let filepath = format!("{0}/{1}", dir, filename);
        Self::create_dir_if_not_exists(dir)?;
        Self::create_file_if_not_exists(dir, filename)?;
        let serialized_config =
            fs::read_to_string(filepath).map_err(|e| Error::FileRead(format!("{:?}", e)))?;
        Ok(serde_json::from_str(&serialized_config)
            .map_err(|e| Error::Deserialization(format!("{:?}", e)))?)
    }

    pub fn save(&self, dir: &str, filename: &str) -> Result<()> {
        let filepath = format!("{0}/{1}", dir, filename);
        Self::create_dir_if_not_exists(dir)?;
        Self::create_file_if_not_exists(dir, filename)?;
        let file = fs::OpenOptions::new()
            .write(true)
            .open(filepath)
            .map_err(|e| Error::FileRead(format!("{:?}", e)))?;
        let mut writer = BufWriter::new(file);
        serde_json::to_writer(&mut writer, &self)
            .map_err(|e| Error::Serialization(format!("{:?}", e)))?;
        writer
            .flush()
            .map_err(|e| Error::FileCreation(format!("{:?}", e)))?;
        Ok(())
    }

    fn create_dir_if_not_exists(dir: &str) -> Result<()> {
        fs::create_dir_all(dir).map_err(|e| Error::DirectoryCreation(format!("{:?}", e)))?;
        Ok(())
    }

    fn create_file_if_not_exists(dir: &str, filename: &str) -> Result<()> {
        let filepath = format!("{0}/{1}", dir, filename);
        match fs::File::open(&filepath) {
            Ok(_) => Ok(()),
            Err(_) => {
                let _ = fs::File::create(filepath)
                    .map_err(|e| Error::FileCreation(format!("{:?}", e)))?;
                Self::create_default_file_values(dir, filename)?;
                Ok(())
            }
        }
    }

    fn create_default_file_values(dir: &str, filename: &str) -> Result<()> {
        Self::new().save(dir, filename)
    }
}
