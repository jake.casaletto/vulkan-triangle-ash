use std::sync::Arc;

use vulkano::instance::debug::DebugUtilsMessengerCreateInfo;
use vulkano::instance::{debug::DebugUtilsMessenger, Instance, InstanceCreateInfo};
use vulkano::VulkanLibrary;

use crate::config::Config;
use crate::vulkan::error::{Error, Result};

pub struct VulkanInstance {
    _inner: Arc<Instance>,
    _debug_callback: Option<DebugUtilsMessenger>,
}

impl VulkanInstance {
    // TODO - don't just use all supported extensions; use required only and then enable as config requires (perf).
    // let required_extensions = vulkano_win::required_extensions(&library);
    pub fn try_new(config: &Config) -> Result<Self> {
        let library =
            VulkanLibrary::new().map_err(|e| Error::VulkanNotSupported(format!("{:?}", e)))?;
        let mut supported_extensions = library.supported_extensions().clone();
        let mut create_info = InstanceCreateInfo::application_from_cargo_toml();

        if !config.vulkan_enable_validation_layers {
            supported_extensions.ext_debug_utils = false;
            supported_extensions.ext_debug_report = false;
            supported_extensions.ext_validation_features = false;
            supported_extensions.ext_validation_flags = false;
        };

        create_info.enabled_extensions = supported_extensions.clone();
        let instance = Instance::new(library, create_info)
            .map_err(|e| Error::InstanceCreation(format!("{:?}", e)))?;

        let debug_messenger = Self::create_debug_messenger(&config, instance.clone())?;

        Ok(Self {
            _inner: instance,
            _debug_callback: debug_messenger,
        })
    }

    fn create_debug_messenger(
        config: &Config,
        instance: Arc<Instance>,
    ) -> Result<Option<DebugUtilsMessenger>> {
        if !config.vulkan_enable_validation_layers {
            return Ok(None);
        };

        let create_info = DebugUtilsMessengerCreateInfo::user_callback(Arc::new(|msg| {
            println!("Debug callback: {:?}", msg.description);
        }));

        Ok(Some(unsafe {
            DebugUtilsMessenger::new(instance, create_info)
                .map_err(|e| Error::DebugMessengerCreation(format!("{:?}", e)))?
        }))
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Arc<Instance> {
        &self._inner
    }
}
