use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("No supporting Vulkan library/DLL found: {0}")]
    VulkanNotSupported(String),
    #[error("Failed to create Vulkan instance: {0}")]
    InstanceCreation(String),
    #[error("Failed to create Debug messenger: {0}")]
    DebugMessengerCreation(String),
    #[error("Failed to find a supported physical device: {0}")]
    PhysicalDevice(String),
    #[error("Failed to create vulkan logical device: {0}")]
    LogicalDevice(String),
    #[error("Error creating Surface: {0}")]
    SurfaceCreation(String),
    #[error("Error accessing Window.")]
    SurfaceWindowAccess,
    #[error("Error creating Swapchain: {0}")]
    SwapchainCreation(String),
    #[error("Error compiling shader: {0}")]
    ShaderCompilation(String),
    #[error("Error creating RenderPass: {0}")]
    RenderPassCreation(String),
    #[error("Error creating Graphics Pipeline: {0}")]
    GraphicsPipelineCreation(String),
    #[error("Error creating Framebuffer: {0}")]
    FramebufferCreation(String),
    #[error("Error creating VertexBuffer: {0}")]
    VertexbufferCreation(String),
    #[error("Error accessing next swapchain image: {0}")]
    SwapchainImageAccess(String),
}
