use std::sync::Arc;

use vulkano::device::{Device, DeviceCreateInfo, Queue, QueueCreateInfo, DeviceExtensions};

use crate::vulkan::error::{Error, Result};

use super::physical::VulkanPhysicalDevice;

// TODO - abstract vulkan dependencies w/ adapter
pub struct VulkanDevice {
    _inner: Arc<Device>,
    _inner_queues: Vec<Arc<Queue>>,
}

impl VulkanDevice {
    pub fn try_new(physical_device: &VulkanPhysicalDevice, device_extensions: &DeviceExtensions) -> Result<Self> {
        let (device, queues) = Device::new(
            physical_device.expose_inner_TODO_REMOVE().clone(),
            DeviceCreateInfo {
                enabled_extensions: device_extensions.clone(),
                queue_create_infos: Self::get_all_device_create_infos(&physical_device),
                ..Default::default()
            },
        )
        .map_err(|e| Error::LogicalDevice(format!("{:?}", e)))?;
        Ok(Self {
            _inner: device,
            _inner_queues: queues.collect(),
        })
    }

    fn get_all_device_create_infos(physical_device: &VulkanPhysicalDevice) -> Vec<QueueCreateInfo> {
        physical_device
            .expose_queue_family_indices_TODO_REMOVE()
            .iter()
            // TODO remove case and replace with better interface
            .map(|queue_family_index| QueueCreateInfo {
                queue_family_index: *queue_family_index as u32,
                ..Default::default()
            })
            .collect()
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Arc<Device> {
        &self._inner
    }

    pub fn expose_inner_queues_TODO_REMOVE(&self) -> &Vec<Arc<Queue>> {
        &self._inner_queues
    }

}
