use std::sync::Arc;

use vulkano::device::{physical::PhysicalDevice, QueueFlags, DeviceExtensions};

use crate::vulkan::{
    error::{Error, Result},
    instance::VulkanInstance,
    surface::VulkanSurface,
};

use super::REQUIRED_QUEUE_FLAGS;

// TODO - abstract vulkan dependencies w/ adapter
pub struct VulkanPhysicalDevice {
    _inner: Arc<PhysicalDevice>,
    _index: usize,
    _queue_family_indices: Vec<usize>,
}

impl VulkanPhysicalDevice {
    pub fn try_new(instance: &VulkanInstance, surface: &VulkanSurface, device_extensions: &DeviceExtensions) -> Result<Self> {
        // TODO - currently picking first usable but we should find *best* or preferred usable physical device
        // https://vulkano.rs/windowing/swapchain_creation.html#optional-checking-for-swapchain-support
        let physical_device_index = instance
            .expose_inner_TODO_REMOVE()
            .enumerate_physical_devices()
            .map_err(|e| Error::PhysicalDevice(format!("{:?}", e)))?
            .filter(|p| p.supported_extensions().contains(device_extensions))
            .position(|d| Self::is_device_usable(&d, surface))
            .ok_or_else(|| Error::PhysicalDevice("No physical devices found".to_string()))?;
        let physical_device = instance
            .expose_inner_TODO_REMOVE()
            .enumerate_physical_devices()
            .expect("Panic! Physical devices could not be found when expected!")
            .nth(physical_device_index)
            .ok_or_else(|| Error::PhysicalDevice("No physical devices found".to_string()))?;
        let queue_family_indices_results = REQUIRED_QUEUE_FLAGS
            .iter()
            .map(|flag| Self::queue_family_index(&physical_device, *flag, surface))
            .collect::<Vec<Result<usize>>>();
        let queue_family_indices = queue_family_indices_results
            .into_iter()
            .collect::<Result<Vec<usize>>>()?;

        Ok(Self {
            _inner: physical_device,
            _index: physical_device_index,
            _queue_family_indices: queue_family_indices,
        })
    }

    // TODO - clean up this dumpster fire of flow control
    fn is_device_usable(physical_device: &Arc<PhysicalDevice>, surface: &VulkanSurface) -> bool {
        for flag in REQUIRED_QUEUE_FLAGS {
            let queue_family_index = Self::queue_family_index(physical_device, flag, surface);
            match queue_family_index {
                Ok(index) => {
                    if physical_device
                        .surface_support(index as u32, &surface.expose_inner_TODO_REMOVE())
                        .is_err()
                    {
                        return false;
                    };
                    let formats = physical_device
                        .surface_formats(surface.expose_inner_TODO_REMOVE(), Default::default());
                    match formats {
                        Ok(formats) => {
                            !formats.is_empty()
                                && physical_device
                                    .surface_present_modes(surface.expose_inner_TODO_REMOVE())
                                    .is_ok()
                        }
                        Err(_) => false,
                    }
                }
                Err(_) => return false,
            };
        }
        true
    }

    fn queue_family_index(
        physical_device: &Arc<PhysicalDevice>,
        required_flag: QueueFlags,
        surface: &VulkanSurface,
    ) -> Result<usize> {
        Ok(physical_device
            .queue_family_properties()
            .iter()
            .enumerate()
            .position(|(i, props)| {
                props.queue_flags.contains(required_flag)
                    && physical_device
                        .surface_support(i as u32, surface.expose_inner_TODO_REMOVE())
                        .is_ok()
            })
            .ok_or(Error::PhysicalDevice(format!(
                "Physical device does not support flag: {:?}",
                required_flag
            )))?)
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Arc<PhysicalDevice> {
        &self._inner
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_queue_family_indices_TODO_REMOVE(&self) -> &Vec<usize> {
        &self._queue_family_indices
    }
}
