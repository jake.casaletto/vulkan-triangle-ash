// TODO - runtime shader compilation, for now these are just going to be made at compilation but that isn't suitable for an engine ap.
// https://github.com/vulkano-rs/vulkano/blob/master/examples/src/bin/runtime-shader/main.rs#L409

use std::sync::Arc;

use vulkano::shader::ShaderModule;

use super::{
    device::logical::VulkanDevice,
    error::{Error, Result},
};

mod vertex_shader {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "assets/triangle.vert"
    }
}

mod fragment_shader {
    vulkano_shaders::shader! {
        ty: "fragment",
        path: "assets/triangle.frag"
    }
}

pub struct VulkanShaders {
    pub verts: Vec<Arc<ShaderModule>>,
    pub frags: Vec<Arc<ShaderModule>>,
}

impl VulkanShaders {
    pub fn try_new(device: &VulkanDevice) -> Result<Self> {
        let vert = vertex_shader::load(device.expose_inner_TODO_REMOVE().clone())
            .map_err(|e| Error::ShaderCompilation(format!("{:?}", e)))?;

        let frag = fragment_shader::load(device.expose_inner_TODO_REMOVE().clone())
            .map_err(|e| Error::ShaderCompilation(format!("{:?}", e)))?;
        Ok(Self {
            verts: vec![vert],
            frags: vec![frag],
        })
    }
}
