use std::sync::Arc;

use vulkano::{
    buffer::{Buffer, BufferCreateInfo, BufferUsage, Subbuffer},
    memory::allocator::{AllocationCreateInfo, MemoryUsage, StandardMemoryAllocator},
};

use crate::{vertex::AppVertex, vulkan::device::logical::VulkanDevice};

use super::command::VulkanCommandBufferAllocator;
use crate::vulkan::error::{Error, Result};

pub struct VulkanVertexBuffer {
    _inner: Arc<Subbuffer<[AppVertex]>>,
}

impl VulkanVertexBuffer {
    pub fn try_new(logical_device: &VulkanDevice, vertices: Vec<AppVertex>) -> Result<Self> {
        let vertex_buffer = Buffer::from_iter(
            &StandardMemoryAllocator::new_default(
                logical_device.expose_inner_TODO_REMOVE().clone(),
            ),
            BufferCreateInfo {
                usage: BufferUsage::VERTEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                usage: MemoryUsage::Upload,
                ..Default::default()
            },
            vertices.into_iter(),
        )
        .map_err(|e| Error::VertexbufferCreation(format!("{:?}", e)))?;

        Ok(Self {
            _inner: Arc::new(vertex_buffer),
        })
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Subbuffer<[AppVertex]> {
        &self._inner
    }
}
