use std::sync::Arc;

use vulkano::render_pass::Framebuffer;
use vulkano::{image::view::ImageView, render_pass::FramebufferCreateInfo};

use crate::vulkan::render_pass::VulkanRenderPass;
use crate::vulkan::swapchain::VulkanSwapchain;
use crate::vulkan::error::{Error, Result};

pub struct VulkanFrameBuffer {
    _inner: Vec<Arc<Framebuffer>>,
}

impl VulkanFrameBuffer {
    pub fn try_new(swapchain: &VulkanSwapchain, render_pass: &VulkanRenderPass) -> Result<Self> {
        let framebuffers = swapchain
            .expose_inner_images_TODO_REMOVE()
            .iter()
            .map(|image| {
                let view =
                    ImageView::new_default(image.clone()).expect("Framebuffer creation error!");
                Framebuffer::new(
                    render_pass.expose_inner_TODO_REMOVE().clone(),
                    FramebufferCreateInfo {
                        attachments: vec![view],
                        ..Default::default()
                    },
                )
                .expect("Framebuffer creation error!")
            })
            .collect::<Vec<_>>();
        Ok(Self {
            _inner: framebuffers,
        })
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Vec<Arc<Framebuffer>> {
        &self._inner
    }
}
