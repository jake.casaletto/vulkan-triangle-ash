use std::ops::DerefMut;
use std::sync::Arc;

use vulkano::command_buffer::allocator::StandardCommandBufferAllocator;
use vulkano::command_buffer::{
    AutoCommandBufferBuilder, CommandBufferUsage, PrimaryAutoCommandBuffer, RenderPassBeginInfo,
    SubpassContents,
};
use vulkano::render_pass::Framebuffer;
use vulkano::{image::view::ImageView, render_pass::FramebufferCreateInfo};

use crate::vulkan::device::logical::VulkanDevice;
use crate::vulkan::pipeline::VulkanGraphicsPipeline;

use super::frame::VulkanFrameBuffer;
use super::vertex::VulkanVertexBuffer;

pub struct VulkanCommandBufferAllocator {
    _inner: StandardCommandBufferAllocator,
}

impl VulkanCommandBufferAllocator {
    pub fn new(logical_device: &VulkanDevice) -> Self {
        Self {
            _inner: StandardCommandBufferAllocator::new(
                logical_device.expose_inner_TODO_REMOVE().clone(),
                Default::default(),
            ),
        }
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &StandardCommandBufferAllocator {
        &self._inner
    }
}

pub struct VulkanCommandBuffer {
    _inner: Vec<Arc<PrimaryAutoCommandBuffer>>,
}

impl VulkanCommandBuffer {
    // TODO error handling and also this is complete nonsense.
    pub fn new(
        allocator: &VulkanCommandBufferAllocator,
        logical_device: &VulkanDevice,
        pipeline: &VulkanGraphicsPipeline,
        framebuffers: &VulkanFrameBuffer,
        vertex_buffer: &VulkanVertexBuffer,
    ) -> Self {
        Self {
            _inner: framebuffers
                .expose_inner_TODO_REMOVE()
                .iter()
                .map(|framebuffer| {
                    let mut builder = AutoCommandBufferBuilder::primary(
                        allocator.expose_inner_TODO_REMOVE(),
                        logical_device
                            .expose_inner_queues_TODO_REMOVE()
                            .first()
                            .expect("Command buffer creation failure!")
                            .queue_family_index(),
                        CommandBufferUsage::MultipleSubmit,
                    )
                    .unwrap();

                    builder
                        .begin_render_pass(
                            RenderPassBeginInfo {
                                clear_values: vec![Some([0.0, 0.0, 1.0, 1.0].into())],
                                ..RenderPassBeginInfo::framebuffer(framebuffer.clone())
                            },
                            SubpassContents::Inline,
                        )
                        .unwrap()
                        .bind_pipeline_graphics(pipeline.expose_inner_TODO_REMOVE().clone())
                        .bind_vertex_buffers(0, vertex_buffer.expose_inner_TODO_REMOVE().clone())
                        .draw(
                            vertex_buffer.expose_inner_TODO_REMOVE().len() as u32,
                            1,
                            0,
                            0,
                        )
                        .unwrap()
                        .end_render_pass()
                        .unwrap();

                    Arc::new(builder.build().unwrap())
                })
                .collect(),
        }
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Vec<Arc<PrimaryAutoCommandBuffer>> {
        &self._inner
    }
}
