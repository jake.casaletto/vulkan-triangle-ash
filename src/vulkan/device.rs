use vulkano::device::{QueueFlags, DeviceExtensions};

pub mod logical;
pub mod physical;

// TODO - expose this in the interface as an injectable dep (params, generic, dont matter)
pub const REQUIRED_QUEUE_FLAGS: [QueueFlags; 1] = [QueueFlags::GRAPHICS];
