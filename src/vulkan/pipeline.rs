use std::sync::Arc;

use vulkano::{
    pipeline::{
        graphics::{
            input_assembly::{InputAssemblyState, PrimitiveTopology},
            vertex_input::{self, VertexDefinition},
            viewport::{Viewport, ViewportState},
            GraphicsPipelineBuilder,
        },
        GraphicsPipeline,
    },
    render_pass::Subpass,
};

use super::{
    device::logical::VulkanDevice,
    error::{Error, Result},
    render_pass::VulkanRenderPass,
    shaders::VulkanShaders,
    swapchain::VulkanSwapchain,
};

pub struct VulkanGraphicsPipeline {
    _inner: Arc<GraphicsPipeline>,
}

impl VulkanGraphicsPipeline {
    pub fn try_new(
        swapchain: &VulkanSwapchain,
        compiled_shaders: &VulkanShaders,
        logical_device: &VulkanDevice,
        render_pass: &VulkanRenderPass,
        viewport: &Viewport,
    ) -> Result<Self> {
        let swapchain_extent = swapchain.expose_inner_TODO_REMOVE().image_extent();

        let pipeline = GraphicsPipeline::start()
            .vertex_shader(
                compiled_shaders.verts[0]
                    .entry_point("main")
                    .expect("Graphics Pipeline Failure"),
                (),
            )
            .fragment_shader(
                compiled_shaders.frags[0]
                    .entry_point("main")
                    .expect("Graphics Pipeline Failure"),
                (),
            )
            .input_assembly_state(InputAssemblyState::topology(
                Default::default(),
                PrimitiveTopology::TriangleList,
            ))
            .viewport_state(ViewportState::viewport_fixed_scissor_irrelevant([
                viewport.clone()
            ]))
            .render_pass(Subpass::from(render_pass.expose_inner_TODO_REMOVE().clone(), 0).unwrap())
            .build(logical_device.expose_inner_TODO_REMOVE().clone())
            .map_err(|e| Error::GraphicsPipelineCreation(format!("{:?}", e)))?;

        Ok(Self { _inner: pipeline })
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Arc<GraphicsPipeline> {
        &self._inner
    }
}
