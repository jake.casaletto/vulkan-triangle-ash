use std::sync::Arc;

use crate::vulkan::error::{Error, Result};
use vulkano::swapchain::Surface;
use vulkano_win::VkSurfaceBuild;
use winit::{
    dpi::LogicalSize,
    window::{Window, WindowBuilder},
};

use crate::{config::Config, main_loop::MainLoop, vulkan::instance::VulkanInstance};

pub struct VulkanSurface {
    _inner: Arc<Surface>,
}

impl VulkanSurface {
    pub fn try_new(
        event_loop: &MainLoop,
        config: &Config,
        instance: &VulkanInstance,
    ) -> Result<Self> {
        Ok(Self {
            _inner: WindowBuilder::new()
                .with_inner_size(LogicalSize::new(config.window_x, config.window_y))
                .with_title(&config.window_title)
                .build_vk_surface(
                    event_loop.expose_inner_TODO_REMOVE(),
                    instance.expose_inner_TODO_REMOVE().clone(),
                )
                .map_err(|e| Error::SurfaceCreation(format!("{:?}", e)))?,
        })
    }

    pub fn expose_inner_TODO_REMOVE(&self) -> &Arc<Surface> {
        &self._inner
    }

    pub fn try_expose_inner_window_TODO_REMOVE(&self) -> Result<Arc<Window>> {
        Ok(self
            ._inner
            .object()
            .ok_or(Error::SurfaceWindowAccess)?
            .clone()
            .downcast::<Window>()
            .map_err(|_| Error::SurfaceWindowAccess)?
            .clone())
    }
}
