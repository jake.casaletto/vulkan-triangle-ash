use std::sync::Arc;

use vulkano::render_pass::RenderPass;

use super::{
    device::logical::VulkanDevice,
    error::{Error, Result},
    swapchain::VulkanSwapchain,
};

pub struct VulkanRenderPass {
    _inner: Arc<RenderPass>,
}

impl VulkanRenderPass {
    pub fn try_new(swapchain: &VulkanSwapchain, device: &VulkanDevice) -> Result<Self> {
        let render_pass = vulkano::single_pass_renderpass!(
            device.expose_inner_TODO_REMOVE().clone(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: swapchain.expose_inner_TODO_REMOVE().image_format(), // set the format the same as the swapchain
                    samples: 1,
                },
            },
            pass: {
                color: [color],
                depth_stencil: {},
            },
        )
        .map_err(|e| Error::RenderPassCreation(format!("{:?}", e)))?;
        Ok(Self {
            _inner: render_pass,
        })
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Arc<RenderPass> {
        &self._inner
    }
}
