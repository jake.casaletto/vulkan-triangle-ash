use std::sync::Arc;

use crate::vulkan::error::{Error, Result};

use vulkano::{
    image::{ImageUsage, SwapchainImage},
    swapchain::{Swapchain, SwapchainCreateInfo},
};

use super::{
    device::{logical::VulkanDevice, physical::VulkanPhysicalDevice},
    surface::VulkanSurface,
};

pub struct VulkanSwapchain {
    _inner: Arc<Swapchain>,
    _images: Vec<Arc<SwapchainImage>>,
}

// TODO  - most of these should be graphics condiguration options
impl VulkanSwapchain {
    pub fn try_new(
        surface: &VulkanSurface,
        physical_device: &VulkanPhysicalDevice,
        logical_device: &VulkanDevice,
    ) -> Result<Self> {
        let capabilities = physical_device
            .expose_inner_TODO_REMOVE()
            .surface_capabilities(surface.expose_inner_TODO_REMOVE(), Default::default())
            .map_err(|e| Error::SwapchainCreation(format!("{:?}", e)))?;

        let dimensions = surface.try_expose_inner_window_TODO_REMOVE()?.inner_size();

        let composite_alpha = capabilities
            .supported_composite_alpha
            .into_iter()
            .next()
            .ok_or(Error::SwapchainCreation(format!(
                "Couldn't access composite alpha capabilities."
            )))?;

        let image_format = physical_device
            .expose_inner_TODO_REMOVE()
            .surface_formats(&surface.expose_inner_TODO_REMOVE(), Default::default())
            .map_err(|e| Error::SwapchainCreation(format!("{:?}", e)))?
            .first()
            .ok_or(Error::SwapchainCreation(format!("No image format found.")))?
            .clone();

        let present_mode = physical_device
            .expose_inner_TODO_REMOVE()
            .surface_present_modes(surface.expose_inner_TODO_REMOVE())
            .map_err(|e| Error::SwapchainCreation(format!("{:?}", e)))?
            .nth(0)
            .ok_or(Error::SwapchainCreation(format!("No present mode found.")))?;

        let (swapchain, images) = Swapchain::new(
            logical_device.expose_inner_TODO_REMOVE().clone(),
            surface.expose_inner_TODO_REMOVE().clone(),
            SwapchainCreateInfo {
                image_color_space: image_format.1,
                image_extent: dimensions.into(),
                image_format: Some(image_format.0),
                min_image_count: capabilities.min_image_count + 1,
                image_usage: ImageUsage::COLOR_ATTACHMENT,
                composite_alpha,
                present_mode,
                ..Default::default()
            },
        )
        .map_err(|e| Error::SwapchainCreation(format!("{:?}", e)))?;

        Ok(Self {
            _inner: swapchain,
            _images: images,
        })
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_TODO_REMOVE(&self) -> &Arc<Swapchain> {
        &self._inner
    }

    // TODO abstract all vulkano dependencies behind p/a
    pub fn expose_inner_images_TODO_REMOVE(&self) -> &Vec<Arc<SwapchainImage>> {
        &self._images
    }

    pub fn recreate_swapchain(original: &Self, surface: &VulkanSurface) -> Result<Self> {
        let new_dimensions = surface.try_expose_inner_window_TODO_REMOVE()?.inner_size();
        let (new_swapchain, images): (Arc<Swapchain>, Vec<Arc<SwapchainImage>>) =
            Swapchain::recreate(
                original.expose_inner_TODO_REMOVE(),
                SwapchainCreateInfo {
                    image_extent: new_dimensions.into(),
                    ..original.expose_inner_TODO_REMOVE().create_info()
                },
            )
            .map_err(|e| Error::SwapchainCreation(format!("{:?}", e)))?;
        Ok(Self {
            _inner: new_swapchain,
            _images: images,
        })
    }
}
